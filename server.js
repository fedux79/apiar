//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/fdiaz/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

var movimientosJSONV2 = require("./movimientosv2.json");

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ("/",function(req, res) {
  res.sendFile(path.join(__dirname,"index.html"));
});

app.get ("/clientes/:idcliente", function(req, res) {
  res.send("Aqui tiene all cliente nro:" + req.params.idcliente);
});

app.get ("/movimientos",function(req, res) {
  clienteMLab.get('',function(err,reqM,body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }

  })
});



app.post("/movimientos",function(req, res) {
  clienteMLab.post('',req.body,function(err,reqM,body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }

  })
});




app.get ("/v1/movimientos",function(req, res) {
  res.sendFile(path.join(__dirname,"movimientosv1.json"));
});

app.get ("/v2/movimientos",function(req, res) {
  res.send(movimientosJSONV2);
});


app.get ("/v2/movimientosq",function(req, res) {
  console.log(req.query);
  res.send();
});

app.get ("/v2/movimientos/:id",function(req, res) {
//  res.send(movimientosJSONV2[req.params.id]);
     res.status(200).send(movimientosJSONV2.filter(el => el.id == req.params.id ));
});


app.post ("/",function(req, res) {
  res.send("Hemos recibido su peticion post cambiada");
});


app.post ("/v2/movimientos",function(req, res) {
  var nuevo=req.body;
  nuevo.id = movimientosJSONV2.length + 1;
  movimientosJSONV2.push(nuevo);
  //res.send("Movimiento dado de alta");
  res.status(200).send("Se agregó el elemento:" + JSON.stringify(nuevo) + "\n EN \n"  +  JSON.stringify(movimientosJSONV2));
});
